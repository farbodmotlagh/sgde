#!/bin/bash

timesToRun=100

echo "x,y"
for((i=1000;i<=5000;i+=100))
do
    amount=$i
    ./generator $timesToRun $amount $amount
    printf "%s, %s\n" $amount $(./firstfit out.txt)
    rm out.txt
    sleep 1
done
