ARGS=-std=c++11 -O2 -g -DTIME

all:
	c++ firstfit.cc -o firstfit $(ARGS)
	c++ bruteforce.cc -o bruteforce $(ARGS)
	c++ generator.cc -o generator $(ARGS)

clean:
	rm -rf firstfit generator bruteforce *.dSYM

