#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>

typedef double Seconds;
inline Seconds GetTimeNow()
{
    using namespace std;

    return chrono::duration_cast<chrono::duration<Seconds, std::ratio<1> > >(chrono::high_resolution_clock::now().time_since_epoch()).count();
}

enum
{
    SUCCESS = 0,
    NO_ITEMS,
    INVALID_BIN_WEIGHT,
    INVALID_ITEM_WEIGHT
};

/*
 * Packs the items into bins of maximum size binSize.
 *
 * @returns        0: Success
 * @returns        1: There are no items to pack
 * @returns        2: The bin weight is <= 0
 * @returns        3: One or more of the item weights are valid (<= 0 | > binSize)
 * @param items    the items to pack
 * @param binSize  the size of the bins
*/
int pack(std::vector<float>& items, float binSize)
{
    if (items.size() == 0)
        return NO_ITEMS;

    if (binSize <= 0)
        return INVALID_BIN_WEIGHT;

    std::vector<float> ret;

    // Turns this into first fit decreasing
    std::sort(items.begin(), items.end(), std::greater<float>());

    for (auto val : items)
    {
        if (val > binSize || val <= 0)
            return INVALID_ITEM_WEIGHT;

        bool hasFoundBin = false;

        for(auto& r : ret)
        {
            if (binSize - r >= val)
            {
                r += val;
                hasFoundBin = true;
                break;
            }
        }

        if (!hasFoundBin)
            ret.push_back(val);
    }

    items = ret;

    return SUCCESS;
}

/*
 * Packs and returns the result.
 *
 * @returns        Failure: <packresult> (see constants above)
 * @returns        Success: <packresult> <numBins> <bin_1> <bin_2> ... <bin_numBins>
*/
void performAndOutput(std::vector<float>& items, float binSize, std::ostream& os)
{
    int result = pack(items, binSize);

#ifndef TIME
    os << result;

    if (result == 0)
    {
        os << ' ' << items.size();

        for(auto& item : items)
            os << ' ' << item;
    }
#endif
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: <filename>\n";
        std::cerr << "Or:\n";
        std::cerr << "Usage: <binsize> <item_1> <item_2> ... <item_n>" << std::endl;
        return 1;
    }

    float binSize = 0;
    std::vector<float> items;

    if (argc == 2)
    {
        // Try load from file

        /*
         * Syntax:
         *
         * <int> Number of instances : I
         * Repeat for all instances:
         * <int> Number of items: N
         * <float> Volume of the bins
         * <float[N]> Weights of each item (N items)
         *
         */

        std::ifstream fstream{argv[1]};
        if (fstream.is_open())
        {
            int numInstances = 0;
            fstream >> numInstances;

            Seconds total = 0;
            for (int i = 0; i < numInstances; i++)
            {
                // Reset
                items.clear();
                binSize = 0;

                int numItems = 0;
                fstream >> numItems;

                fstream >> binSize;

                for (int n = 0; n < numItems; n++)
                {
                    float f = 0;
                    fstream >> f;
                    items.push_back(f);
                }

                auto t1 = GetTimeNow();
                performAndOutput(items, binSize, std::cout);
                auto t2 = GetTimeNow();
                total += (t2-t1);
            }

            Seconds time = total / numInstances;
            std::cout << time << std::endl;
            return 0;
        }
    }

    // Read from args
    binSize = atof(argv[1]);

    for (int i = 2; i < argc; i++)
        items.push_back(atof(argv[i]));

#ifdef TIME
    auto t1 = GetTimeNow();
    performAndOutput(items, binSize, std::cout);
    auto t2 = GetTimeNow();
    std::cout << (t2-t1) << '\n';
#else
    performAndOutput(items, binSize, std::cout);
    std::cout << std::endl;
#endif
}
