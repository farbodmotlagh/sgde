#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <string>
#include <sstream>
#include <fstream>

enum
{
    SUCCESS = 0,
    NO_ITEMS,
    INVALID_BIN_WEIGHT,
    INVALID_ITEM_WEIGHT,
    FAIL
};

int packRecursive(std::vector<float>& items, std::vector<float>& bins, float binSize, int index)
{
    if (items.size() == index) {
      return SUCCESS;
    }
    for (int i = 0; i < bins.size(); i++) {
      //std::cout << "Let's put (" << index << "|" << items[index]  << ")" << " into (" << i << "|" << bins[i] << ")" << std::endl;
      if (bins[i] + items[index] > binSize) {
        //std::cout << "Can't" << std::endl;
        continue;
      } else {
        //std::cout << "Ok" << std::endl;
        bins[i] += items[index];

        int result = packRecursive(items, bins, binSize, index+1);
        if (result == SUCCESS) {
          //std::cout << "SUCCESS" << std::endl;
          return SUCCESS;
        }
      //std::cout << "Let's take (" << index << "|" << items[index]  << ")" << " out of (" << i << "|" << bins[i] << ")" << std::endl;
        bins[i] -= items[index];
      }
    }

    //std::cout << "FAIL at " << index << std::endl;
    return FAIL;
}

/*
 * Packs the items into bins of maximum size binSize.
 *
 * @returns        0: Success
 * @returns        1: There are no items to pack
 * @returns        2: The bin weight is <= 0
 * @returns        3: One or more of the item weights are valid (<= 0 | > binSize)
 * @param items    the items to pack
 * @param binSize  the size of the bins
*/
int pack(std::vector<float>& items, float binSize)
{
    if (items.size() == 0)
        return NO_ITEMS;

    if (binSize <= 0)
        return INVALID_BIN_WEIGHT;

    for (auto val : items)
    {
      if (val > binSize || val <= 0)
        return INVALID_ITEM_WEIGHT;
    }


    int binNum = 1;
    while(true) {
      //std::cout << binNum << std::endl;
      std::vector<float> bins(binNum, 0);
      int result = packRecursive(items, bins, binSize, 0);
      if (result != SUCCESS) {
        binNum++;
      } else {
        items = bins;
        return SUCCESS;
      }
    }
}


/*
 * Packs and returns the result.
 *
 * @returns        Failure: <packresult> (see constants above)
 * @returns        Success: <packresult> <numBins> <bin_1> <bin_2> ... <bin_numBins>
*/
std::string performAndOutput(std::vector<float>& items, float binSize)
{
    int result = pack(items, binSize);

    std::stringstream ret;
    ret << result;

    if (result == 0)
    {
        ret << ' ' << items.size();

        for(auto& item : items)
            ret << ' ' << item;
    }

    return ret.str();
}

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Usage: <filename>\n";
        std::cerr << "Or:\n";
        std::cerr << "Usage: <binsize> <item_1> <item_2> ... <item_n>" << std::endl;
        return 1;
    }

    float binSize = 0;
    std::vector<float> items;

    if (argc == 2)
    {
        // Try load from file

        /*
         * Syntax:
         *
         * <int> Number of instances : I
         * Repeat for all instances:
         * <int> Number of items: N
         * <float> Volume of the bins
         * <float[N]> Weights of each item (N items)
         *
         */

        std::ifstream fstream{argv[1]};
        if (fstream.is_open())
        {
            int numInstances = 0;
            fstream >> numInstances;

            for (int i = 0; i < numInstances; i++)
            {
                // Reset
                items.clear();
                binSize = 0;

                int numItems = 0;
                fstream >> numItems;

                fstream >> binSize;

                for (int n = 0; n < numItems; n++)
                {
                    float f = 0;
                    fstream >> f;
                    items.push_back(f);
                }

                std::cout << performAndOutput(items, binSize) << std::endl;
            }

            // We've read from file, can exit here
            return 0;
        }
    }

    // Read from args
    binSize = atof(argv[1]);

    for (int i = 2; i < argc; i++)
        items.push_back(atof(argv[i]));

    std::cout << performAndOutput(items, binSize) << std::endl;
}
