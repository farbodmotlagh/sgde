#include <cstdlib>
#include <iostream>
#include <sstream>
#include <cfloat>
#include <vector>
#include <fstream>
#include <algorithm>
#include <ctime>

const int MAX_VOLUME = 10000;

float sum(std::vector<float> elements)
{
	float f = 0;
	for (auto& e : elements)
		f += e;
	return f;
}

void genProblem(std::stringstream& stream, int n)
{
	if (n <= 1)
	{
		std::cerr << "Impossible to generate a non-degenerate solution for the given n" << std::endl;
		return;
	}

	int volume = rand() % MAX_VOLUME + 1;

	stream << n << '\n' << volume << '\n';

	std::vector<float> elements;

	while (sum(elements) <= volume)
	{
		elements.clear();

		for (int i = 1; i <= n; i++)
			elements.push_back(rand() % volume + 1);
	}

	for (int i = 0; i < elements.size(); i++)
	{
		if (i != 0)
			stream << ' ';
		stream << elements[i];
	}
	stream << '\n';
}

int main(int argc, char* argv[])
{
	srand(time(NULL));

	int numInstances = 1;
	int minItems = 1;
	int maxItems = 1;

	if (argc < 2)
	{
		std::cerr << "Usage: <items>\n";
		std::cerr << "Or:\n" << std::endl;
		std::cerr << "Usage: <numInstances> <minItems> <maxItems>" << std::endl;
		return 1;
	}

	switch (argc)
	{
		default:
		case 2:
			numInstances = 1;
			minItems = maxItems = atoi(argv[1]);
			break;
		case 4:
			numInstances = atoi(argv[1]);
			minItems = atoi(argv[2]);
			maxItems = atoi(argv[3]);
			break;
	}

	// Sanity checks:
	numInstances = std::max(1, numInstances);
	minItems = std::max(1, minItems);
	maxItems = std::max(1, maxItems);
	minItems = std::min(minItems, maxItems);
	maxItems = std::max(minItems, maxItems);


	std::stringstream ss;
	ss << numInstances << '\n';

	while (numInstances-- > 0)
		genProblem(ss, (rand() % (maxItems - minItems + 1)) + minItems);

	std::ofstream fstream;
	fstream.open("out.txt");
	fstream << ss.str();
}
